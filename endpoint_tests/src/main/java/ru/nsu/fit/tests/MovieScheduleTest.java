package ru.nsu.fit.tests;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Test;
import ru.nsu.fit.services.CalendarService;
import ru.nsu.fit.services.browser.Browser;
import ru.nsu.fit.services.browser.BrowserService;
import ru.nsu.fit.services.screens.MainPageScreen;
import ru.nsu.fit.shared.Logger;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Title;
import ru.yandex.qatools.allure.model.SeverityLevel;

public class MovieScheduleTest
{
    @SuppressWarnings("Since15")
    @Test
    @Title("Movie schedule test")
    @Description("Movie schedule test")
    @Severity(SeverityLevel.NORMAL)
    @Features("Movie schedule feature")
    public void movieSchedule()
    {
        try (Browser browser = BrowserService.openNewBrowser())
        {
            MainPageScreen.open(browser);

            String expectedDate = CalendarService.getKinopoiskDate(2);
            MainPageScreen.selectDate(browser, expectedDate);
            String actualDate = MainPageScreen.openSchedule(browser);

            Assert.assertEquals(actualDate, expectedDate);
            Logger.log("Opening movie schedule on selected day is OK :)", Logger.TEST_RESULT);
        }
    }
}
