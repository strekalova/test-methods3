package ru.nsu.fit.tests;

import org.testng.annotations.Test;
import ru.nsu.fit.services.LoginService;
import ru.nsu.fit.services.browser.Browser;
import ru.nsu.fit.services.browser.BrowserService;
import ru.nsu.fit.services.screens.MainPageScreen;
import ru.nsu.fit.services.screens.ProfilePageScreen;
import ru.nsu.fit.shared.Logger;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Title;
import ru.yandex.qatools.allure.model.SeverityLevel;

/**
 * Created by Svetlana on 20.12.2016.
 */
public class LoginTest
{
    @Test
    @Title("Kinopoisk login")
    @Description("Login by Kinopoisk user")
    @Severity(SeverityLevel.BLOCKER)
    @Features("Login feature")
    public void login()
    {
        String login = LoginService.getLogin();
        String pass = LoginService.getPassword();
        try (Browser browser = BrowserService.openNewBrowser())
        {
            Logger.log("Try to login with username " + login + " and password " + pass, Logger.INFO);

            MainPageScreen.open(browser);
            LoginService.loginKP(browser);
            MainPageScreen.openProfilePage(browser, login);

            ProfilePageScreen.checkPage(browser, login);
            Logger.log("Login by " + login + " is OK :)", Logger.TEST_RESULT);
        }
    }
}
