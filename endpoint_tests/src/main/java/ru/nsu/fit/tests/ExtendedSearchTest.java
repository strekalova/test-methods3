package ru.nsu.fit.tests;

import org.openqa.selenium.By;
import org.openqa.selenium.internal.Locatable;
import org.testng.annotations.Test;
import ru.nsu.fit.services.browser.Browser;
import ru.nsu.fit.services.browser.BrowserService;
import ru.nsu.fit.services.screens.ExtendedSearchScreen;
import ru.nsu.fit.services.screens.FilmPageScreen;
import ru.nsu.fit.services.screens.MainPageScreen;
import ru.nsu.fit.shared.AllureUtils;
import ru.nsu.fit.shared.Logger;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Title;
import ru.yandex.qatools.allure.model.SeverityLevel;

public class ExtendedSearchTest
{
    @Test
    @Title("Extended film search")
    @Description("Extended film search by title and year")
    @Severity(SeverityLevel.NORMAL)
    @Features("Search feature")
    public void extendedSearch()
    {
        try (Browser browser = BrowserService.openNewBrowser())
        {
            MainPageScreen.open(browser);
            MainPageScreen.openExtendedSearch(browser);
            ExtendedSearchScreen.typeFilmName(browser, "Кэрри");
            ExtendedSearchScreen.typeFilmYear(browser, "1976");
            ExtendedSearchScreen.clickSearch(browser);
            FilmPageScreen.checkDirector(browser, "Брайан Де Пальма", "\"Carrie, 1976\"");

            Logger.log("Extended search is OK :)", Logger.TEST_RESULT);
        }
    }
}
