package ru.nsu.fit.tests;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Test;
import ru.nsu.fit.services.LoginService;
import ru.nsu.fit.services.browser.Browser;
import ru.nsu.fit.services.browser.BrowserService;
import ru.nsu.fit.services.screens.MainPageScreen;
import ru.nsu.fit.services.screens.ProfilePageScreen;
import ru.nsu.fit.shared.Logger;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Title;
import ru.yandex.qatools.allure.model.SeverityLevel;

public class CheckListTest {
    @Test
    @Title("Kinopoisk check list")
    @Description("Kinopoisk check list")
    @Severity(SeverityLevel.NORMAL)
    @Features("List of films feature")
    public void checkList()
    {
        String login = LoginService.getLogin();
        try (Browser browser = BrowserService.openNewBrowser())
        {
            MainPageScreen.open(browser);
            LoginService.loginKP(browser);

            MainPageScreen.openProfilePage(browser, login);
            ProfilePageScreen.openListsOfFilms(browser);
            ProfilePageScreen.openList(browser);
            ProfilePageScreen.checkList(browser);

            Logger.log("List checking is OK", Logger.TEST_RESULT);
        }
    }
}
