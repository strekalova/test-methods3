package ru.nsu.fit.tests;

import org.openqa.selenium.By;
import org.testng.annotations.Test;
import ru.nsu.fit.services.LoginService;
import ru.nsu.fit.services.browser.Browser;
import ru.nsu.fit.services.browser.BrowserService;
import ru.nsu.fit.services.screens.MainPageScreen;
import ru.nsu.fit.shared.Logger;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Title;
import ru.yandex.qatools.allure.model.SeverityLevel;

/**
 * Created by Svetlana on 20.12.2016.
 */
public class LogoutTest
{
    @Test
    @Title("Kinopoisk logout")
    @Description("Logout by Kinopoisk user")
    @Severity(SeverityLevel.BLOCKER)
    @Features("Logout feature")
    public void logout()
    {
        try (Browser browser = BrowserService.openNewBrowser())
        {
            MainPageScreen.open(browser);
            LoginService.loginKP(browser);
            String login = LoginService.getLogin();
            MainPageScreen.signOut(browser);
            Logger.log("Logout by " + login + " is OK :)", Logger.TEST_RESULT);
        }
    }
}
