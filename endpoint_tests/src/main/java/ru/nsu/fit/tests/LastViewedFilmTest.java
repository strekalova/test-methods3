package ru.nsu.fit.tests;

import org.testng.Assert;
import org.testng.annotations.Test;
import ru.nsu.fit.services.browser.Browser;
import ru.nsu.fit.services.browser.BrowserService;
import ru.nsu.fit.services.screens.FilmPageScreen;
import ru.nsu.fit.services.screens.MainPageScreen;
import ru.nsu.fit.services.screens.SearchResultsPageScreen;
import ru.nsu.fit.shared.Logger;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Title;
import ru.yandex.qatools.allure.model.SeverityLevel;

public class LastViewedFilmTest {
    @Test
    @Title("Kinopoisk last viewed film")
    @Description("Kinopoisk last viewed film")
    @Severity(SeverityLevel.NORMAL)
    @Features("Last viewed film feature")
    public void lastViewed() {
        try (Browser browser = BrowserService.openNewBrowser())
        {
            MainPageScreen.open(browser);
            MainPageScreen.searchFilm(browser, "Deadpool");
            SearchResultsPageScreen.openFirstResult(browser);
            FilmPageScreen.getTitle(browser, "Deadpool film page");

            MainPageScreen.open(browser);

            String expectedTitle = "Дэдпул (Deadpool, 2016)";
            String actualTitle = MainPageScreen.getLastViewedFilmTitle(browser);

            Assert.assertEquals(actualTitle, expectedTitle);
            Logger.log("Last viewed pages is OK", Logger.TEST_RESULT);
        }
    }
}
