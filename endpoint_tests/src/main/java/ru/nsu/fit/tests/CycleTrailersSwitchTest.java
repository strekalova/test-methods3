package ru.nsu.fit.tests;

import org.testng.annotations.Test;
import ru.nsu.fit.services.browser.Browser;
import ru.nsu.fit.services.browser.BrowserService;
import ru.nsu.fit.services.screens.MainPageScreen;
import ru.nsu.fit.shared.Logger;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Title;
import ru.yandex.qatools.allure.model.SeverityLevel;

import java.util.TreeSet;

public class CycleTrailersSwitchTest {

    @Test
    @Title("Kinopoisk cycle trailers switching")
    @Description("Kinopoisk cycle trailers switching")
    @Severity(SeverityLevel.MINOR)
    @Features("Cycle switch feature")
    public void switchTrailers()
    {
        try (Browser browser = BrowserService.openNewBrowser())
        {
            MainPageScreen.open(browser);
            MainPageScreen.scrollToTrailers(browser);

            TreeSet<String> names = new TreeSet<>();
            String curName = "";
            int iters = 0;
            while (true) {
                curName = MainPageScreen.getAnotherTrailer(browser);
                if (!names.contains(curName))
                    names.add(curName);
                else
                    break;

                if (iters > 100)
                    break;
                iters++;
            }
        }

        Logger.log("Trailer switching is OK", Logger.TEST_RESULT);
    }
}
