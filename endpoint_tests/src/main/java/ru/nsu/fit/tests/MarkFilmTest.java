package ru.nsu.fit.tests;


import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.reporters.jq.Main;
import ru.nsu.fit.services.LoginService;
import ru.nsu.fit.services.browser.Browser;
import ru.nsu.fit.services.browser.BrowserService;
import ru.nsu.fit.services.screens.FilmPageScreen;
import ru.nsu.fit.services.screens.MainPageScreen;
import ru.nsu.fit.services.screens.ProfilePageScreen;
import ru.nsu.fit.services.screens.SearchResultsPageScreen;
import ru.nsu.fit.shared.Logger;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Title;
import ru.yandex.qatools.allure.model.SeverityLevel;

public class MarkFilmTest {

    @Test
    @Title("Kinopoisk film rating")
    @Description("Kinopoisk film rating")
    @Severity(SeverityLevel.NORMAL)
    @Features("Film rating feature")
    public void rateFilm()
    {
        try (Browser browser = BrowserService.openNewBrowser())
        {
            MainPageScreen.open(browser);
            LoginService.loginKP(browser);
            MainPageScreen.searchFilm(browser, "Кавказская пленница!");
            SearchResultsPageScreen.openFirstResult(browser);

            FilmPageScreen.rateFilm(browser, 1);
            FilmPageScreen.goToRating(browser);

            String name = ProfilePageScreen.getLastRatedFilmName(browser);
            Assert.assertEquals("Кавказская пленница! (2014)", name);
            Logger.log("Film is rated", Logger.INFO);
            Logger.saveImage("Film is rated", browser.makeScreenshot());

            String mark = ProfilePageScreen.getLastRatedFilmMark(browser);
            Assert.assertEquals("1", mark);
            Logger.log("Film is rated by 1 star", Logger.INFO);
            Logger.saveImage("Film is rated by 1 star", browser.makeScreenshot());
            Logger.log("Film rating is OK", Logger.TEST_RESULT);
        }
    }
}
