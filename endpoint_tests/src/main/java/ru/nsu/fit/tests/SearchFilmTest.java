package ru.nsu.fit.tests;

import org.testng.Assert;
import org.testng.annotations.Test;
import ru.nsu.fit.services.browser.Browser;
import ru.nsu.fit.services.browser.BrowserService;
import ru.nsu.fit.services.screens.FilmPageScreen;
import ru.nsu.fit.services.screens.MainPageScreen;
import ru.nsu.fit.services.screens.SearchResultsPageScreen;
import ru.nsu.fit.shared.Logger;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Title;
import ru.yandex.qatools.allure.model.SeverityLevel;

public class SearchFilmTest {

    @Test
    @Title("Kinopoisk film search")
    @Description("Kinopoisk film search")
    @Severity(SeverityLevel.CRITICAL)
    @Features("Search feature")
    public void searchFilm()
    {
        try (Browser browser = BrowserService.openNewBrowser())
        {
            MainPageScreen.open(browser);
            MainPageScreen.searchFilm(browser, "John Wick");
            SearchResultsPageScreen.openFirstResult(browser);

            String actualTitle = FilmPageScreen.getTitle(browser, "John Wick film page");
            String expectedTitle = "Джон Уик (2014) — КиноПоиск";
            Assert.assertEquals(actualTitle, expectedTitle);
            Logger.log("Search film " + "John Wick" + " is OK :)", Logger.TEST_RESULT);
        }
    }
}
