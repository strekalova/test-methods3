package ru.nsu.fit.tests;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Test;
import ru.nsu.fit.services.browser.Browser;
import ru.nsu.fit.services.browser.BrowserService;
import ru.nsu.fit.services.screens.FilmPageScreen;
import ru.nsu.fit.services.screens.MainPageScreen;
import ru.nsu.fit.services.screens.SearchResultsPageScreen;
import ru.nsu.fit.shared.Logger;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Title;
import ru.yandex.qatools.allure.model.SeverityLevel;

public class SearchActorTest {
    @Test
    @Title("Kinopoisk actor search")
    @Description("Kinopoisk actor search")
    @Severity(SeverityLevel.CRITICAL)
    @Features("Search feature")
    public void searchActor()
    {
        try (Browser browser = BrowserService.openNewBrowser())
        {
            MainPageScreen.open(browser);
            MainPageScreen.searchFilm(browser, "Keanu Reeves");
            SearchResultsPageScreen.openFirstResult(browser);

            String actualTitle = FilmPageScreen.getTitle(browser, "Keanu Reeves actor page");
            String expectedTitle = "Киану Ривз — фильмы — КиноПоиск";

            Assert.assertEquals(actualTitle, expectedTitle);
            Logger.log("Search actor " + "Keanu Reeves" + " is OK :)", Logger.TEST_RESULT);
        }
    }
}
