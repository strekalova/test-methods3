package ru.nsu.fit.shared;

/**
 * Created by Svetlana on 20.12.2016.
 */
public class Logger
{
    public static final String INFO = "[INFO] ";
    public static final String WARNING = "[WARNING] ";
    public static final String ERROR = "[ERROR] ";
    public static final String TEST_RESULT = "[TEST RESULT] ";

    public static void log(String message, String messageType)
    {
        String msg = messageType + message;
        AllureUtils.saveTextLog(msg);
        System.out.println(msg);
    }

    public static void saveImage(String imageName, byte[] image)
    {
        AllureUtils.saveImageAttach(imageName, image);
    }
}
