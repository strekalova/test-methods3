package ru.nsu.fit.services.screens;

import org.openqa.selenium.By;
import org.openqa.selenium.internal.Locatable;
import ru.nsu.fit.services.browser.Browser;
import ru.nsu.fit.shared.AllureUtils;
import ru.nsu.fit.shared.Logger;

public class MainPageScreen {

    private final static By arrowNext = By.xpath("//*[@id=\"main_editorial\"]/table[3]/tbody/tr[1]/td/div[1]/span[2]");
    private final static By nameEl = By.xpath("//*[@id=\"main_editorial\"]/table[3]/tbody/tr[1]/td/div[1]/div[2]/div[1]/a");

    public static void open(Browser browser)   {
        browser.openPage("https://www.kinopoisk.ru/");
        Logger.log("Opened Kinopoisk main page", Logger.INFO);
        Logger.saveImage("Kinopoisk main page", browser.makeScreenshot());
    }

    public static void openLoginForm(Browser browser)    {
        browser.waitForElement(By.xpath("//*[@id=\"top_form\"]/div[2]/a[1]"));
        Logger.saveImage("Kinopoisk", browser.makeScreenshot());
        browser.click(By.xpath("//*[@id=\"top_form\"]/div[2]/a[1]"));

        browser.switchFrame("kp2-authapi-iframe");
        browser.waitForElement(By.linkText("Регистрация"));
        Logger.log("Appeared popup login frame", Logger.INFO);
        Logger.saveImage("Kinopoisk login popup", browser.makeScreenshot());
    }

    public static void typeLogin(Browser browser, String login)    {
        browser.typeText(By.name("login"), login);
        Logger.log("Login printed", Logger.INFO);
        Logger.saveImage("Kinopoisk login popup: login printed", browser.makeScreenshot());
    }

    public static void typePassword(Browser browser, String password)    {
        browser.typeText(By.name("password"), password);
        Logger.log("Password printed", Logger.INFO);
        Logger.saveImage("Kinopoisk login popup: password printed", browser.makeScreenshot());
    }

    public static void clickLogin(Browser browser)    {
        while (!browser.getElement(By.xpath("/html/body/div/div[2]/div/form/div[1]/div[1]/button")).isEnabled()) {}
        browser.click(By.xpath("/html/body/div[1]/div[2]/div/form/div[1]/div[1]/button/span"));
        Logger.log("Login done", Logger.INFO);
        Logger.saveImage("Kinopoisk login popup: button clicked", browser.makeScreenshot());
    }

    public static void openProfilePage(Browser browser, String login) {
        browser.waitForElement(By.linkText("Профиль: " + login));
        Logger.log("Kinopoisk main page by signed up user", Logger.INFO);
        Logger.saveImage("Kinopoisk main page: user inside", browser.makeScreenshot());
        browser.click(By.linkText("Профиль: " + login));
    }

    public static void signOut(Browser browser) {
        browser.waitForElement(By.xpath("//*[@id=\"top_form\"]/div[3]/a"));
        Logger.log("Kinopoisk main page by signed up user", Logger.INFO);
        Logger.saveImage("Kinopoisk main page: user inside", browser.makeScreenshot());

        browser.click(By.xpath("//*[@id=\"top_form\"]/div[3]/a"));
        Logger.log("Kinopoisk main page by unsigned user", Logger.INFO);

        browser.waitForElement(By.xpath("//*[@id=\"top_form\"]/div[2]/a[1]"));
        Logger.saveImage("Kinopoisk main page: user outside", browser.makeScreenshot());
    }

    public static void scrollToTrailers(Browser browser) {
        browser.waitForElement(arrowNext);
        ((Locatable) browser.getElement(By.xpath("//*[@id=\"main_editorial\"]/table[3]/tbody/tr[1]/td/div[1]"))).getCoordinates();
        Logger.log("Kinopoisk main page", Logger.INFO);
        Logger.saveImage("Kinopoisk main page", browser.makeScreenshot());
    }

    public static String getAnotherTrailer(Browser browser) {
        Logger.log("Next trailer", Logger.INFO);
        Logger.saveImage("Another trailer", browser.makeScreenshot());
        String curName = browser.getElement(nameEl).getText();
        browser.click(arrowNext);
        return curName;
    }

    public static void openExtendedSearch(Browser browser) {
        By extended = By.linkText("расширенный поиск");
        browser.waitForElement(extended);
        browser.click(extended);
        By titleInput = By.id("find_film");
        browser.waitForElement(titleInput);
        Logger.log("Opened extended search page", Logger.INFO);
        AllureUtils.saveImageAttach("Extended search page", browser.makeScreenshot());
    }

    public static void searchFilm(Browser browser, String s) {
        browser.waitForElement(By.xpath("//*[@id=\"search_input\"]"));
        Logger.log("Kinopoisk main page", Logger.INFO);
        Logger.saveImage("Kinopoisk main page", browser.makeScreenshot());
        browser.typeText(By.xpath("//*[@id=\"search_input\"]"), s);
        browser.click(By.xpath("//*[@id=\"top_form\"]/input[3]"));
    }

    public static void selectDate(Browser browser, String expectedDate) {
        By days = By.xpath("//*[@id=\"block_right\"]/dl[1]/dd[1]/form/select");
        browser.waitForElement(days);
        browser.click(days);
        Logger.log("Opened dates drop-down list", Logger.INFO);
        Logger.saveImage("Dates drop-down list", browser.makeScreenshot());

        By day = By.xpath("//*[@id=\"block_right\"]/dl[1]/dd[1]/form/select/option[3]");
        browser.waitForElement(day);
        browser.click(day);
        Logger.log("Date \"" + expectedDate +  "\" selected", Logger.INFO);
        Logger.saveImage("Date selected", browser.makeScreenshot());
    }

    public static String openSchedule(Browser browser) {
        browser.click(By.linkText("расписание сеансов"));
        By date = By.xpath("//*[@id=\"block_left\"]/div/table/tbody/tr/td/div[8]/div[1]");
        browser.waitForElement(date);
        String actualDate = browser.getElement(date).getText();
        Logger.log("Opened page on date " + actualDate, Logger.INFO);
        Logger.saveImage("Opened schedule on selected date", browser.makeScreenshot());
        return actualDate;
    }

    public static String getLastViewedFilmTitle(Browser browser) {
        browser.waitForElement(By.xpath("//*[@id=\"last_film\"]/ul/li[1]/a/img"));
        ((Locatable) browser.getElement(By.linkText("iPhone & iPad"))).getCoordinates();
        Logger.log("Kinopoisk main page: last viewed pages", Logger.INFO);
        Logger.saveImage("Kinopoisk main page: last viewed pages", browser.makeScreenshot());
        return browser.getElement(By.xpath("//*[@id=\"last_film\"]/ul/li[1]/a/img")).getAttribute("title");
    }
}
