package ru.nsu.fit.services;

import org.openqa.selenium.By;
import ru.nsu.fit.services.browser.Browser;
import ru.nsu.fit.services.browser.BrowserService;
import ru.nsu.fit.services.screens.MainPageScreen;

public class LoginService {
    private static final String login = "SkySerenity";
    private static final String pass = "VeryStrongPass16226";
    private static Browser browser = null;

    public static void loginKP(Browser browser) {
        MainPageScreen.openLoginForm(browser);
        MainPageScreen.typeLogin(browser, login);
        MainPageScreen.typePassword(browser, pass);
        MainPageScreen.clickLogin(browser);
    }
    public static String getLogin() {
        return login;
    }

    public static String getPassword() {
        return pass;
    }
}
