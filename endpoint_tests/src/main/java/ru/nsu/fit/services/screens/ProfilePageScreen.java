package ru.nsu.fit.services.screens;

import org.openqa.selenium.By;
import org.testng.Assert;
import ru.nsu.fit.services.browser.Browser;
import ru.nsu.fit.shared.Logger;

public class ProfilePageScreen {

    public static void checkPage(Browser browser, String login)    {
        Logger.log("Clicked profile page button", Logger.INFO);
        browser.waitForElement(By.className("nick_name"));
        Logger.saveImage(login + " profile page", browser.makeScreenshot());

        String expectedTitle = "Профиль: " + login;
        String actualTitle = browser.getTitle();
        Logger.log("Actual page title: " + actualTitle + ", expected: " + expectedTitle, Logger.INFO);
        Assert.assertEquals(actualTitle, expectedTitle);
    }

    public static void openListsOfFilms(Browser browser)
    {
        browser.waitForElement(By.xpath("//*[@id=\"content_block\"]/div/ul/li[8]/a"));
        Logger.log("Profile page", Logger.INFO);
        Logger.saveImage("Profile page", browser.makeScreenshot());
        browser.click(By.xpath("//*[@id=\"content_block\"]/div/ul/li[8]/a"));
    }

    public static void openList(Browser browser)
    {
        browser.waitForElement(By.xpath("//*[@id=\"block_left_pad\"]/div/div[3]/ul/li[2]/div"));
        Logger.log("Open lists of films page", Logger.INFO);
        Logger.saveImage("Open lists of films page", browser.makeScreenshot());
        browser.click(By.xpath("//*[@id=\"block_left_pad\"]/div/div[3]/ul/li[2]/div"));
    }

    public static void checkList(Browser browser)
    {
        browser.waitForElement(By.xpath("//*[@id=\"block_left_pad\"]/div/div[2]/div/h1"));
        Logger.log("Page with films about vampires", Logger.INFO);
        Logger.saveImage("Page with films about vampires", browser.makeScreenshot());
        String title = browser.getElement(By.xpath("//*[@id=\"block_left_pad\"]/div/div[2]/div/h1")).getText();
        Assert.assertEquals("Фильмы про вампиров", title);
    }

    public static String getLastRatedFilmName(Browser browser)
    {
        browser.waitForElement(By.xpath("//*[@id=\"list\"]/tbody/tr[4]/td/div/div[2]/div[2]/div[2]/div[1]"));
        return browser.getElement(By.xpath("//*[@id=\"list\"]/tbody/tr[4]/td/div/div[2]/div[2]/div[2]/div[1]")).getText();
    }


    public static String getLastRatedFilmMark(Browser browser) {
        browser.waitForElement(By.xpath("//*[@id=\"list\"]/tbody/tr[4]/td/div/div[2]/div[2]/div[4]"));
        return browser.getElement(By.xpath("//*[@id=\"list\"]/tbody/tr[4]/td/div/div[2]/div[2]/div[4]")).getText();
    }
}
