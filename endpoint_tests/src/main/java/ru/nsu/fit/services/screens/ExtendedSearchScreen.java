package ru.nsu.fit.services.screens;

import org.openqa.selenium.By;
import org.openqa.selenium.internal.Locatable;
import ru.nsu.fit.services.browser.Browser;
import ru.nsu.fit.shared.AllureUtils;
import ru.nsu.fit.shared.Logger;

public class ExtendedSearchScreen {

    public static void typeFilmName(Browser browser, String film) {
        By titleInput = By.id("find_film");
        browser.waitForElement(titleInput);
        browser.typeText(titleInput, film);
        Logger.log("Typed film title", Logger.INFO);
        AllureUtils.saveImageAttach("Typed film title", browser.makeScreenshot());
    }

    public static void typeFilmYear(Browser browser, String year) {
        browser.waitForElement(By.id("year"));
        browser.typeText(By.id("year"), year);
        Logger.log("Typed film year", Logger.INFO);
        AllureUtils.saveImageAttach("Typed film year", browser.makeScreenshot());
    }

    public static void clickSearch(Browser browser)
    {
        browser.waitForElement(By.xpath("//*[@id=\"formSearchMain\"]/input[11]"));
        ((Locatable) browser.getElement(By.xpath("//*[@id=\"formSearchMain\"]/input[11]"))).getCoordinates();
        browser.click(By.xpath("//*[@id=\"formSearchMain\"]/input[11]"));
    }
}
