package ru.nsu.fit.services;

import java.util.Calendar;
import java.util.TimeZone;

/**
 * Created by Svetlana on 18.12.2016.
 */
public class CalendarService
{
    private static String[] months = {"января", "февраля", "марта", "апреля", "мая", "июня", "июля", "августа", "сентября", "октября", "ноября", "декабря"};
    private static String[] daysOfWeek = {"Воскресенье", "Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота"};

    public static String getKinopoiskDate(int daysAfterToday)
    {
        Calendar today = Calendar.getInstance(TimeZone.getTimeZone("UTC+07:00"));
        today.roll(Calendar.DATE, daysAfterToday);

        String dayOfWeek = daysOfWeek[today.get(Calendar.DAY_OF_WEEK) - 1];
        int day = today.get(Calendar.DAY_OF_MONTH);
        String month = months[today.get(Calendar.MONTH)];

        return dayOfWeek + ", " + day + " " + month;
    }
}
