package ru.nsu.fit.services.screens;

import org.openqa.selenium.By;
import ru.nsu.fit.services.browser.Browser;
import ru.nsu.fit.shared.Logger;

public class SearchResultsPageScreen {

    public static void openFirstResult(Browser browser) {
        browser.waitForElement(By.xpath("//*[@id=\"block_left_pad\"]/div/div[3]/div/div[2]/p/a"));
        Logger.log("Search page with film", Logger.INFO);
        Logger.saveImage("Search page with film", browser.makeScreenshot());
        browser.click(By.xpath("//*[@id=\"block_left_pad\"]/div/div[3]/div/div[2]/p/a"));
    }
}
