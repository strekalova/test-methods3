package ru.nsu.fit.services.screens;

import org.openqa.selenium.By;
import ru.nsu.fit.services.browser.Browser;
import ru.nsu.fit.shared.AllureUtils;
import ru.nsu.fit.shared.Logger;

public class FilmPageScreen {

    public static void checkDirector(Browser browser, String prodName, String info) {
        By director = By.linkText(prodName);
        browser.waitForElement(director);
        Logger.log("Opened "+ info + " film page", Logger.INFO);
        AllureUtils.saveImageAttach("Film page", browser.makeScreenshot());
    }

    public static void rateFilm(Browser browser, int i) {
        browser.waitForElement(By.className("moviename-big"));
        Logger.log("Open film page", Logger.INFO);

        browser.click(By.xpath("//*[@id=\"film_votes\"]/div[1]/div/a[" + (11 - i) + "]"));
        Logger.log("Rate film by 1 star", Logger.INFO);
        Logger.saveImage("Rate film by 1 star", browser.makeScreenshot());
    }

    public static void goToRating(Browser browser)    {
        browser.waitForElement(By.xpath("//*[@id=\"my_vote_block\"]/a"));
        browser.click(By.xpath("//*[@id=\"my_vote_block\"]/a"));
    }

    public static String getTitle(Browser browser, String info) {
        browser.waitForElement(By.className("moviename-big"));
        Logger.log(info, Logger.INFO);
        Logger.saveImage(info, browser.makeScreenshot());
        return browser.getTitle();
    }
}
